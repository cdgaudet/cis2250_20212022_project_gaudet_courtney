package info.hccis.cis2250_20212022_project_gaudet_courtney.entity;

/**
 * TutorRepository
 *
 * @author BJM/CIS2250 modified by CDG
 * @since 20220129
 */

import info.hccis.cis2250_20212022_project_gaudet_courtney.api.ApiWatcher;
import info.hccis.cis2250_20212022_project_gaudet_courtney.api.JsonTutorApi;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class TutorRepository {

    private static TutorRepository instance;

    private JsonTutorApi jsonTutorApi;

    public static TutorRepository getInstance() {
        if (instance == null) {
            instance = new TutorRepository();
        }
        return instance;
    }

    public TutorRepository() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiWatcher.API_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonTutorApi = retrofit.create(JsonTutorApi.class);
    }

    public JsonTutorApi getTutorService() {
        return jsonTutorApi;
    }

}
