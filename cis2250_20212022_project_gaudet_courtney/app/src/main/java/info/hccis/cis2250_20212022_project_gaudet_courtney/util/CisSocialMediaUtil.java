package info.hccis.cis2250_20212022_project_gaudet_courtney.util;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;

import java.util.List;


/**
 * Functionality for social media integration
 *
 * @author bjmaclean
 * @since 20220202
 */
public class CisSocialMediaUtil {

    public static final String PACKAGE_TWITTER = "com.twitter.android";

    public static Intent shareOnSocialMedia(Activity activity, String shareUsingPackage, String subject, String message) {

        // Intent "ACTION_SEND" will be used to send the information from one activity to another
        Intent intent = new Intent(Intent.ACTION_SEND);
        // Setting the type of input that is sending to the application
        intent.setType("text/plain");

        PackageManager packageManager = activity.getPackageManager();
        List<ResolveInfo> resolveInfoList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;

        //Creating a subject line
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);

        //Placing the text entered into the input
        intent.putExtra(Intent.EXTRA_TEXT, message);

        for (ResolveInfo resolveInfo : resolveInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith(shareUsingPackage)) {
                intent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }
        if (!resolved) {
            Log.d("BJM", "could not resolve app");
            intent = intent.createChooser(intent, "Share message using");
        }

        return intent;

    }
}