package info.hccis.cis2250_20212022_project_gaudet_courtney.ui.splash;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import info.hccis.cis2250_20212022_project_gaudet_courtney.ui.main.MainActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Sleeping for a bit to let user usee the splash screen.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //startActivity(new Intent(SplashActivity.this, GoogleSignInActivity.class));
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();
    }
}

