package info.hccis.cis2250_20212022_project_gaudet_courtney.entity;

import android.util.Log;

import java.util.List;

import info.hccis.cis2250_20212022_project_gaudet_courtney.ui.main.MainActivity;

public class TutorContent {

    /**
     * This method will take the list passed in and reload the room database
     * based on the items in the list.
     *
     * @param tutors
     * @author BJM modified by CDG
     * @since 20220210
     */
    public static void reloadTutorsInRoom(List<Tutor> tutors) {
        MainActivity.getMyAppDatabase().tutorDAO().deleteAll();
        for (Tutor current : tutors) {
            MainActivity.getMyAppDatabase().tutorDAO().insert(current);
        }
        Log.d("CG Room", "Loading Tutors into Room");
    }

    /**
     * This method will obtain all the tutors out of the Room database.
     *
     * @return list of tutors
     * @author BJM
     * @since 20220210
     */

    public static List<Tutor> getTutorsFromRoom() {
        Log.d("CG Room", "Loading Tutors from Room");

        List<Tutor> tutorsBack = MainActivity.getMyAppDatabase().tutorDAO().selectAllTutors();
        Log.d("CG Room", "Number of tutors loaded from Room: " + tutorsBack.size());
        for (Tutor current : tutorsBack) {
            Log.d("CG Room", current.toString());
        }
        return tutorsBack;
    }

}