package info.hccis.cis2250_20212022_project_gaudet_courtney.api;

import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import info.hccis.cis2250_20212022_project_gaudet_courtney.R;
import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.Tutor;
import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.TutorContent;
import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.TutorViewModel;
import info.hccis.cis2250_20212022_project_gaudet_courtney.fragments.ViewTutorsFragment;
import info.hccis.cis2250_20212022_project_gaudet_courtney.ui.main.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ApiWatcher class will be used as a background thread which will monitor the api. It will notify
 * the ui activity if the number of rows changes.
 *
 * @author BJM modified by Mariana Alkabalan 20210402 remodified by BJM 20220202 & Courtney Gaudet
 * @since 20210329
 */

public class ApiWatcher extends Thread {

    public static final String API_BASE_URL = "http://10.0.2.2:8081/api/TutorService/";

    private int lengthLastCall = -1;  //Number of rows returned

    //Activity passed in to allow the runOnUIThread to be used.
    private FragmentActivity activity = null;

    public void setActivity(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void run() {
        super.run();
        try {
            do {

                Log.d("CG api", "running");

                //Use Retrofit to connect to the service
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiWatcher.API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                JsonTutorApi jsonTutorApi = retrofit.create(JsonTutorApi.class);

                //Create a list of tutors.
                Call<List<Tutor>> call = jsonTutorApi.getTutors();

                TutorViewModel tutorViewModel = new ViewModelProvider(activity).get(TutorViewModel.class);

                call.enqueue(new Callback<List<Tutor>>() {

                    @Override
                    public void onResponse(@NonNull Call<List<Tutor>> call, @NonNull Response<List<Tutor>> response) {
                        Log.d("CG api", "found tutor view model. size=" + tutorViewModel.getTutors().size());

                        if (!response.isSuccessful()) {
                            Log.d("CG api", "CG not successful response from rest for tutors Code=" + response.code());
                            loadFromRoomIfPreferred(tutorViewModel);
                            lengthLastCall = -1;

                        } else {
                            //Taking the list and pass to constructor of ArrayList to convert it.
                            ArrayList<Tutor> tutorsTemp = new ArrayList(response.body());
                            int lengthThisCall = tutorsTemp.size();

                            Log.d("CG api", "back from api, size=" + lengthThisCall);


                            if (lengthLastCall == -1) {
                                //first time - don't notify
                                lengthLastCall = lengthThisCall;
                                tutorViewModel.setTutors(tutorsTemp); //Will addAll
                                Log.d("CG api ", "First load of tutors from the api");

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("CG api", "trying to notify adapter that things have changed");
                                        ViewTutorsFragment.notifyDataChanged("Found more rows");
                                    }
                                });

                                //******************************************************************
                                //Save latest orders in the database.
                                //******************************************************************

                                TutorContent.reloadTutorsInRoom(tutorsTemp);

                            } else if (lengthThisCall != lengthLastCall) {
                                //******************************************************************
                                //data has changed
                                //******************************************************************
                                Log.d("CG api", "Data has changed");
                                lengthLastCall = lengthThisCall;
                                tutorViewModel.getTutors().clear();
                                tutorViewModel.getTutors().addAll(tutorsTemp);

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("CG api", "Notifying adapter that things have changed");
                                        Log.d("CG api", "Using method to send a notification to the user");
                                        ViewTutorsFragment.notifyDataChanged("Update - the data has changed", activity, MainActivity.class);
                                    }
                                });

                                //******************************************************************
                                //Save latest orders in the database.
                                //******************************************************************
                                TutorContent.reloadTutorsInRoom(tutorsTemp);

                            } else {
                                //*******************************************************************
                                // Same number of rows so don't bother updating the list.
                                //*******************************************************************
                                Log.d("CG api", "Data has not changed");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Tutor>> call, Throwable t) {

                        //**********************************************************************************
                        // If the api call failed, give a notification to the user.
                        //**********************************************************************************
                        Log.d("CG api", "api call failed");
                        Log.d("CG api", t.getMessage());

                        //******************************************************************************************
                        // Using the default shared preferences.  Using the application context - may want to access the
                        // shared prefs from other activities.
                        //******************************************************************************************

                        lengthLastCall = -1; //Indicate couldn't load from api will trigger reload next time
                        loadFromRoomIfPreferred(tutorViewModel);

                    }
                });

                //***********************************************************************************
                // Sleep so not checking all the time
                //***********************************************************************************
                final int SLEEP_TIME = 10000;
                Log.d("CG Sleep", "Sleeping for " + (SLEEP_TIME / 1000) + " seconds");
                Thread.sleep(SLEEP_TIME); //Check api every 10 seconds

            } while (true);
        } catch (InterruptedException e) {
            Log.d("CG api", "Thread interrupted.  Stopping in the thread.");
        }
    }

    /**
     * Check the shared preferences and load from the db if the setting is set to do such a thing.
     *
     * @param tutorViewModel
     * @author BJM
     * @since 20220211
     */
    public void loadFromRoomIfPreferred(TutorViewModel tutorViewModel) {
        Log.d("CG room", "Check to see if should load from room");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        boolean loadFromRoom = sharedPref.getBoolean(activity.getString(R.string.preference_load_from_room), true);
        Log.d("CG room", "Load from Room=" + loadFromRoom);
        if (loadFromRoom) {
            List<Tutor> testList = MainActivity.getMyAppDatabase().tutorDAO().selectAllTutors();
            Log.d("CG room", "Obtained tutors from the db: " + testList.size());
            tutorViewModel.setTutors(testList); //Will add all tutors

            //**********************************************************************
            // This method will allow a call to the runOnUiThread which will be allowed
            // to interact with the ui components of the app.
            //**********************************************************************
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("CG api", "trying to notify adapter that things have changed");
                    ViewTutorsFragment.notifyDataChanged("Found more rows");
                }

            });

        }
    }

}

