package info.hccis.cis2250_20212022_project_gaudet_courtney.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;

/**
 * Class used for providing Broadcast Services
 *
 * @author Courtney Gaudet
 * @since 20220324
 */

public class BCTutorServices extends BroadcastReceiver {
    @RequiresApi(api = Build.VERSION_CODES.O)

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {

            Intent serviceIntent = new Intent(context, FGTutorServices.class);
            context.startForegroundService(serviceIntent);

        }
    }
}
