package info.hccis.cis2250_20212022_project_gaudet_courtney.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.Tutor;

/**
 * Database/RoomDatabase functionality
 *
 * @author bjmaclean modified by Courtney Gaudet
 * @since 20220202
 */

@Database(entities = {Tutor.class}, version = 1)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract TutorDAO tutorDAO();
}

