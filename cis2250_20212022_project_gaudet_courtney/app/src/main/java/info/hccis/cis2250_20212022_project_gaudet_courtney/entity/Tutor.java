package info.hccis.cis2250_20212022_project_gaudet_courtney.entity;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Tutor class based off of Tutor Admin web app, for building Tutor
 *
 * @author bjmaclean modified by Courtney Gaudet
 * @since 20220202
 */

@Entity(tableName = "Tutor")
public class Tutor implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @Size (max = 40)
    private Integer id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String emailAddress;
    private String address;
    private Integer employeeType;

    //Constructors, Getters and Setters

    public Tutor() {
    }
    public Tutor(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(Integer employeeType) {
        this.employeeType = employeeType;
    }

//    /**
//     * Return if a valid employee type or not
//     * @return true if valid
//     * @since 20220117
//     * @author BJM
//     */
//    public boolean validateEmployeeType(){
//        if(employeeType > 0 && employeeType <= 10 ){
//            return true;
//        }else{
//            return false;
//        }
//    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//         if (!(object instanceof Tutor)) {
//            return false;
//        }
//        Tutor other = (Tutor) object;
//        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
//    }


//    @NonNull
//    @Override
//    public String toString() {
//        return "Tutor" + System.lineSeparator()
//                + "Name: " + firstName + " " + lastName + System.lineSeparator()
//                + "Phone Number: " + phoneNumber + System.lineSeparator()
//                + "Email: " + emailAddress + System.lineSeparator()
//                + "Address " + address + System.lineSeparator()
//                + "Employee Type: " + employeeType + System.lineSeparator();
//    }
}
