package info.hccis.cis2250_20212022_project_gaudet_courtney.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import info.hccis.cis2250_20212022_project_gaudet_courtney.ui.main.MainActivity;
import info.hccis.cis2250_20212022_project_gaudet_courtney.R;
import info.hccis.cis2250_20212022_project_gaudet_courtney.adapter.CustomAdapterTutor;
import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.Tutor;
import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.TutorViewModel;
import info.hccis.cis2250_20212022_project_gaudet_courtney.databinding.FragmentViewTutorsBinding;
import info.hccis.cis2250_20212022_project_gaudet_courtney.util.NotificationApplication;
import info.hccis.cis2250_20212022_project_gaudet_courtney.util.NotificationUtil;

/**
 * AddTutorFragment
 * This Java class is associated with the add tutor fragment.  It works with the controls defined
 * on the res/layout/fragment_add_tutor layout file
 *
 * @author BJM/CIS2250 modified by CDG
 * @since 20220129
 */

public class ViewTutorsFragment extends Fragment {

    private static Context context;
    private FragmentViewTutorsBinding binding;
    private List<Tutor> tutorArrayList;
    private static RecyclerView recyclerView;

    public static RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public static void notifyDataChanged(String message) {
        Log.d("CG", "Data changed:  " + message);
        //Send a notification that the data has changed.
        try {
            recyclerView.getAdapter().notifyDataSetChanged();
        } catch (Exception e) {
            Log.d("CG api", "Exception when trying to notify that the data set as changed");
        }
    }

    /**
     * Provide notification tha the data has changed.  This method will notify the adapter that the
     * rows have changed so it will know to refresh.  It will also send a notification to the user which
     * will allow them to go directly back to the list from another activity of the app.
     *
     * @param message          Message to display
     * @param activity         - originating activity
     * @param destinationClass - class associated with the intent associated with the notification.
     */
    public static void notifyDataChanged(String message, Activity activity, Class destinationClass) {
        Log.d("CG", "Data changed:  " + message);
        try {
            notifyDataChanged(message);
            NotificationApplication.setContext(context);
            NotificationUtil.sendNotification("TutorAdmin Data Update", message, activity, MainActivity.class);
        } catch (Exception e) {
            Log.d("CG notification", "Exception occurred when notifying. " + e.getMessage());
        }


    }


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentViewTutorsBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //************************************************************************************
        // Corresponding to the add fragment, the view model is accessed to obtain a reference
        // to the list of tutor bo objects.
        //************************************************************************************
        TutorViewModel tutorViewModel = new ViewModelProvider(getActivity()).get(TutorViewModel.class);

        //************************************************************************************
        //************************************************************************************

        String output = "";
        //double total = 0;
        for (Tutor tutor : tutorViewModel.getTutors()) {
            output += tutor.toString() + System.lineSeparator();
        }

        //************************************************************************************
        // Set the context to be used when sending notifications
        //************************************************************************************

        context = getView().getContext();

        //************************************************************************************
        // Setup the recycler view for displaying the items in the tutor list.
        //************************************************************************************

        recyclerView = binding.recyclerView;

        tutorArrayList = tutorViewModel.getTutors();
        setAdapter();

        //************************************************************************************
        // Button sends the user back to the add fragment
        //************************************************************************************

        binding.buttonAddTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ViewTutorsFragment.this)
                        .navigate(R.id.action_ViewTutorsFragment_to_AddTutorFragment);
            }
        });
    }

    /**
     * Set the adapter for the recyclerview
     *
     * @author BJM
     * @since 20220129
     */
    private void setAdapter() {
        CustomAdapterTutor adapter = new CustomAdapterTutor(tutorArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}