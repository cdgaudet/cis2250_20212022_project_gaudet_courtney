package info.hccis.cis2250_20212022_project_gaudet_courtney.entity;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * TutorViewModel
 *
 * @author BJM/CIS2250 modified by CDG
 * @since 20220129
 */

public class TutorViewModel extends ViewModel {

    private List<Tutor> tutors = new ArrayList();

    public List<Tutor> getTutors() {
        return tutors;
    }

    public void setTutors(List<Tutor> tutors) {
        this.tutors.clear();
        this.tutors.addAll(tutors);
    }
}
