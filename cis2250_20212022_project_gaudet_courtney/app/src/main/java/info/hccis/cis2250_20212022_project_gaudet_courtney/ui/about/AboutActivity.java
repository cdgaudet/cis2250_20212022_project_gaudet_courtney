package info.hccis.cis2250_20212022_project_gaudet_courtney.ui.about;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import info.hccis.cis2250_20212022_project_gaudet_courtney.R;
import info.hccis.cis2250_20212022_project_gaudet_courtney.util.CisSocialMediaUtil;

/**
 * About activity with social media functions
 *
 * @author bjmaclean
 * @since 20220202
 */

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        EditText editTextMessage = findViewById(R.id.editTextMessage);

        Button buttonSendMessage = findViewById(R.id.buttonSendMessage);
        Activity thisActivity = this;
        buttonSendMessage.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = editTextMessage.getText().toString();

                //******************************************************************************
                //Social Media
                //******************************************************************************

                startActivity(CisSocialMediaUtil.shareOnSocialMedia(thisActivity, CisSocialMediaUtil.PACKAGE_TWITTER, "Tutor Admin App", message));

            }
        });


    }
}