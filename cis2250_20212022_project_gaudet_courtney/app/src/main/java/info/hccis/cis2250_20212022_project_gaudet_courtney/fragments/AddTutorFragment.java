package info.hccis.cis2250_20212022_project_gaudet_courtney.fragments;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import info.hccis.cis2250_20212022_project_gaudet_courtney.R;
import info.hccis.cis2250_20212022_project_gaudet_courtney.databinding.FragmentAddTutorBinding;
import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.Tutor;
import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.TutorRepository;
import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.TutorViewModel;
import info.hccis.cis2250_20212022_project_gaudet_courtney.util.ContentProviderUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * AddTutorFragment
 * This Java class is associated with the add tutor fragment.  It works with the controls defined
 * on the res/layout/fragment_add_tutor layout file
 *
 * @author BJM/CIS2250 modified by CDG
 * @since 20220129
 */

public class AddTutorFragment extends Fragment {

    public static final String KEY = "info.hccis.cis2250_20212022_project_gaudet_courtney.Tutor";
    private FragmentAddTutorBinding binding;
    private Tutor tutor;
    private TutorViewModel tutorViewModel;
    private TutorRepository tutorRepository;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("AddTutorFragment CG", "onCreate triggered");
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        Log.d("AddTutorFragment CG", "onCreateView triggered");
        binding = FragmentAddTutorBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("AddTutorFragment CG", "onViewCreated triggered");

        tutorViewModel = new ViewModelProvider(getActivity()).get(TutorViewModel.class);

        //******************************************************************************
        // Content Providers
        // Check permission for user to use the calendar provider.
        //******************************************************************************

        final int callbackId = 42;
        ContentProviderUtil.checkPermission(getActivity(), callbackId, Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR);

        tutorRepository = TutorRepository.getInstance();
        //************************************************************************************
        // Add a listener on the button.
        //************************************************************************************

        binding.buttonSubmitTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("AddTutorFragment CG", "Add Tutor was clicked");

                try {
                    //************************************************************************************
                    // Call the addTutor method
                    //************************************************************************************

                    addTutor();

                    //************************************************************************************
                    // TutorBO implements Serializable.
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(KEY, tutor);

                    //******************************************************************************
                    // Handle the new tutor
                    // Use the post service to add the new tutor to the web database.
                    //******************************************************************************

                    tutorRepository.getTutorService().createTutor(tutor).enqueue(new Callback<Tutor>() {
                        @Override
                        public void onResponse(Call<Tutor> call, Response<Tutor> r) {
                            Toast.makeText(getContext(), "Tutor was successfully added!", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Tutor> call, Throwable t) {
                            Toast.makeText(getContext(), "Error: Adding Tutor unsuccessful: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });


                    //******************************************************************************
                    //Call the post rest api web service to have the new tutor added to the database.
                    //******************************************************************************

                    Snackbar.make(view, "New tutor added.  Adding this new tutor to the database. " +
                            "How should we handle this?", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    //******************************************************************************
                    //Create the event without using Calendar intent
                    //******************************************************************************

                    long eventID = ContentProviderUtil.createEvent(getActivity(), tutor.toString());
                    Toast.makeText(getActivity(), "Calendar Event Created (" + eventID + ")", Toast.LENGTH_SHORT);
                    Log.d("CG Calendar", "Calendar Event Created (" + eventID + ")");

                    //Using an intent
                    ContentProviderUtil.createCalendarEventIntent(getActivity(), tutor.toString());


                    //******************************************************************************
                    // Send a broadcast.
                    //******************************************************************************

                    //Send a broadcast.
                    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(view.getContext());
                    Intent intent = new Intent();
                    intent.setAction("info.hccis.cis2250_20212022_project_gaudet_courtney.tutor");
                    lbm.sendBroadcast(intent);
                    getActivity().sendBroadcast(intent);


                    //************************************************************************************
                    // Navigate to the view orders fragment.  Note that the navigation is defined in
                    // the nav_graph.xml file.
                    //************************************************************************************

                    NavHostFragment.findNavController(AddTutorFragment.this)
                            .navigate(R.id.action_AddTutorFragment_to_ViewTutorsFragment, bundle);
                } catch (Exception e) {
                    //************************************************************************************
                    // If there was an exception thrown in the add tutor method, just put a message
                    // in the Logcat and leave the user on the add fragment.
                    //************************************************************************************
                    Log.d("AddTutorFragment CG", "Error adding: " + e.getMessage());
                }
            }
        });

    }

    /**
     * Add a tutor based on the controls on the view.
     *
     * @throws Exception Throw exception if information entered caused an issue.
     * @author CIS2250
     * @since 20220118
     */
    public void addTutor() throws Exception {

        Log.d("CDG-MainActivity", "First Name: " + binding.editTextTutorFirstName.getText().toString());
        Log.d("CDG-MainActivity", "Last Name: " + binding.editTextTutorLastName.getText().toString());
        Log.d("CDG-MainActivity", "Phone Number: " + binding.editTextTutorPhoneNumber.getText().toString());
        Log.d("CDG-MainActivity", "Email Address: " + binding.editTextTutorEmailAddress.getText().toString());
        Log.d("CDG-MainActivity", "Address: " + binding.editTextTutorAddress.getText().toString());
        Log.d("CDG-MainActivity", "Employee Type: " + binding.editTextTutorEmployeeType.getText().toString());
        Log.d("CDG-MainActivity", "Submit Tutor button was clicked.");

        tutor = new Tutor();
        tutor.setFirstName(binding.editTextTutorFirstName.getText().toString());
        tutor.setLastName(binding.editTextTutorLastName.getText().toString());
        tutor.setPhoneNumber(binding.editTextTutorPhoneNumber.getText().toString());
        tutor.setEmailAddress(binding.editTextTutorEmailAddress.getText().toString());
        tutor.setAddress(binding.editTextTutorEmailAddress.getText().toString());

        int employeeType = 0;
        try {
            employeeType = Integer.parseInt(binding.editTextTutorEmployeeType.getText().toString());
        } catch (Exception e) {
            employeeType = 0;
        }

        tutor.setEmployeeType(employeeType);


    }

}