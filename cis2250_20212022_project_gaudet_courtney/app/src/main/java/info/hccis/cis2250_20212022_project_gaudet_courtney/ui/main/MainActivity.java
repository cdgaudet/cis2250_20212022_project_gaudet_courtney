package info.hccis.cis2250_20212022_project_gaudet_courtney.ui.main;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.room.Room;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.material.snackbar.Snackbar;

import info.hccis.cis2250_20212022_project_gaudet_courtney.R;
import info.hccis.cis2250_20212022_project_gaudet_courtney.api.ApiWatcher;
import info.hccis.cis2250_20212022_project_gaudet_courtney.dao.MyAppDatabase;
import info.hccis.cis2250_20212022_project_gaudet_courtney.databinding.ActivityMainBinding;
import info.hccis.cis2250_20212022_project_gaudet_courtney.receiver.TutorBroadcastReceiver;
import info.hccis.cis2250_20212022_project_gaudet_courtney.services.FGTutorServices;
import info.hccis.cis2250_20212022_project_gaudet_courtney.ui.about.AboutActivity;
import info.hccis.cis2250_20212022_project_gaudet_courtney.ui.google.GoogleSignInActivity;
import info.hccis.cis2250_20212022_project_gaudet_courtney.ui.settings.SettingsActivity;

/**
 * The main activity is used for the core business functionality for this app.  It contains a location
 * to have a fragment on it's view.
 * <p>
 * The main activity does not need to change much.  The menu and floating action button code is defined here
 * but the add/view order coding is contained in the fragments.
 *
 * @author BJM
 * @edited CDG
 * @since 20220129
 */

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;
    private static ApiWatcher apiWatcher;
    private final TutorBroadcastReceiver tutorBroadcastReceiver = new TutorBroadcastReceiver();

    private static final int RC_SIGN_IN = 100;
    GoogleSignInClient mGoogleSignInClient;


    //Room Database
    private static MyAppDatabase myAppDatabase;


    //Provide a getter for the database to be used throughout the app.
    public static MyAppDatabase getMyAppDatabase() {
        return myAppDatabase;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        //************************************************************************************
        // Listener for FAB here
        //************************************************************************************

        binding.fab.setOnClickListener(view -> Snackbar.make(view, "Click the Add Tutor button to place a new tutor in the database.", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());

        apiWatcher = new ApiWatcher();
        apiWatcher.setActivity(this);
        apiWatcher.start();  //Start the background thread

        //****************************************************************************************
        //Set the database attribute
        //****************************************************************************************
        myAppDatabase = Room.databaseBuilder(getApplicationContext(), MyAppDatabase.class, "tutorAppDB").allowMainThreadQueries().build();

        //**************************************************************************************
        //register receiver
        //**************************************************************************************
        IntentFilter filter = new IntentFilter();
        filter.addAction("info.hccis.tutorApp.tutor");
        //registerReceiver(exampleReceiver, filter);
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        lbm.registerReceiver(tutorBroadcastReceiver, filter);

        //Foreground Code with Broad Cast Receiver
        if(!foregroundServiceRunning()) {
            Intent serviceIntent = new Intent(this, FGTutorServices.class);
            startForegroundService(serviceIntent);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        Log.d("CG receiver", "unregistering");
        lbm.unregisterReceiver(tutorBroadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Code here for reacting to user selecting menu items.
     *
     * @return true/false
     * @author BJM
     * @since 20220129
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Log.d("MainActivity CG", "Option selected Settings");
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_about) {
            Log.d("MainActivity CG", "Option selected About");
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_googleSignIn) {
            Log.d("MainActivity CG", "Option selected Google Sign in");
            Intent intent = new Intent(this, GoogleSignInActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public boolean foregroundServiceRunning(){
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for(ActivityManager.RunningServiceInfo service: activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if(FGTutorServices.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


}