package info.hccis.cis2250_20212022_project_gaudet_courtney.ui.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import info.hccis.cis2250_20212022_project_gaudet_courtney.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Switch switch_button = (Switch) findViewById(R.id.switchLoadCustomersFromRoom);

        //******************************************************************************************
        // Using the default shared preferences.  Using the application context - may want to access the
        // shared prefs from other activities.
        //******************************************************************************************

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean loadFromRoom = sharedPref.getBoolean(getString(R.string.preference_load_from_room), true);
        switch_button.setChecked(loadFromRoom);

        //*********************************************************************************
        // Set a checked change listener for switch button
        //*********************************************************************************

        switch_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // If the switch button is on
                    // Show the switch button checked status as toast message
                    Toast.makeText(getApplicationContext(),
                            "Will load from local database if no network", Toast.LENGTH_LONG).show();

                } else {
                    // If the switch button is off
                    // Show the switch button checked status as toast message
                    Toast.makeText(getApplicationContext(),
                            "Will NOT load from local database if no network", Toast.LENGTH_LONG).show();
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.preference_load_from_room), isChecked);
                editor.commit();

            }
        });


    }


}