package info.hccis.cis2250_20212022_project_gaudet_courtney.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import info.hccis.cis2250_20212022_project_gaudet_courtney.R;
import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.Tutor;

/**
 * Adapter for api integration
 *
 * @author bjmaclean modified by Courtney Gaudet
 * @since 20220202
 */
public class CustomAdapterTutor extends RecyclerView.Adapter<CustomAdapterTutor.TutorViewHolder> {

    private List<Tutor> tutorArrayList;

    public CustomAdapterTutor(List<Tutor> tutorBOArrayList) {
        this.tutorArrayList = tutorBOArrayList;
    }


    @NonNull
    @Override
    public TutorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_tutor, parent, false);
        return new TutorViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull TutorViewHolder holder, int position) {

        String firstName = "" + tutorArrayList.get(position).getFirstName();
        String lastName = "" + tutorArrayList.get(position).getLastName();
        String phoneNumber = "" + tutorArrayList.get(position).getPhoneNumber();
        String emailAddress = "" + tutorArrayList.get(position).getEmailAddress();
        String address = "" + tutorArrayList.get(position).getAddress();
        String employeeType = "" + tutorArrayList.get(position).getEmployeeType();

        holder.textViewTutorFirstName.setText(firstName);
        holder.textViewTutorLastName.setText(lastName);
        holder.textViewPhoneNumber.setText(phoneNumber);
        holder.textViewEmailAddress.setText(emailAddress);
        holder.textViewAddress.setText(address);
        holder.textViewEmployeeType.setText(employeeType);

    }

    @Override
    public int getItemCount() {
        return tutorArrayList.size();
    }

    public class TutorViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewTutorFirstName;
        private TextView textViewTutorLastName;
        private TextView textViewPhoneNumber;
        private TextView textViewEmailAddress;
        private TextView textViewAddress;
        private TextView textViewEmployeeType;


        public TutorViewHolder(final View view) {
            super(view);
            textViewTutorFirstName = view.findViewById(R.id.textViewTutorFirstNameListItem);
            textViewTutorLastName = view.findViewById(R.id.textViewTutorLastNameListItem);
            textViewPhoneNumber = view.findViewById(R.id.textViewTutorPhoneNumber);
            textViewEmailAddress = view.findViewById(R.id.textViewTutorEmailAddress);
            textViewAddress = view.findViewById(R.id.textViewTutorAddressListItem);
            textViewEmployeeType = view.findViewById(R.id.textViewTutorEmployeeTypeListItem);
        }
    }


}
