package info.hccis.cis2250_20212022_project_gaudet_courtney.api;

import java.util.List;

import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.Tutor;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface JsonTutorApi {

    /**
     * This abstract method to be created to allow retrofit to get list of tutors
     *
     * @return List of tutors
     * @author BJM modified by Courtney Gaudet (with help from the retrofit research!).
     * @since 20220202
     */

    @GET("tutor")
    Call<List<Tutor>> getTutors();

    @POST("tutor")
    Call<Tutor> createTutor(@Body Tutor tutor);

}