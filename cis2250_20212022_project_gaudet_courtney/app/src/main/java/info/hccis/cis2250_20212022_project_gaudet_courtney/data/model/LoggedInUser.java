package info.hccis.cis2250_20212022_project_gaudet_courtney.data.model;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 *
 * @author bjmaclean modified by Courtney Gaudet
 * @since 20220202
 */
public class LoggedInUser {

    private String userId;
    private String displayName;

    public LoggedInUser(String userId, String displayName) {
        this.userId = userId;
        this.displayName = displayName;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }
}