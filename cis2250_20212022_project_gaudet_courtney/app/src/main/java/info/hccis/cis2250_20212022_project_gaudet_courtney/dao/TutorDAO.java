package info.hccis.cis2250_20212022_project_gaudet_courtney.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import info.hccis.cis2250_20212022_project_gaudet_courtney.entity.Tutor;

/**
 * TutorDAO function, queries for database
 *
 * @author bjmaclean modified by Courtney Gaudet
 * @since 20220202
 */

@Dao
public interface TutorDAO {

    @Insert
    void insert(Tutor tutor);

    @Query("SELECT * FROM Tutor")
    List<Tutor> selectAllTutors();

    @Query("delete from Tutor")
    void deleteAll();

}
