package info.hccis.cis2250_20212022_project_gaudet_courtney.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Class used to setup app for providing Broadcast Receiver function
 *
 * @author Courtney Gaudet
 * @since 20220329
 */

public class TutorBroadcastReceiver extends BroadcastReceiver {
    public TutorBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        boolean isAirplaneModeEnabled = intent.getBooleanExtra("state", false);

        if(isAirplaneModeEnabled){
            Toast.makeText(context, "Airplane Mode Enabled", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(context,"Airplane Mode Disabled", Toast.LENGTH_LONG).show();
        }

        Toast.makeText(context, "Action: " + intent.getAction(), Toast.LENGTH_SHORT).show();
        Log.d("CG receiver", "Tutor Created...Action: " + intent.getAction());
    }
}